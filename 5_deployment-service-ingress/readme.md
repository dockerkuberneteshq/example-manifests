# サンプルYAMLについて

## 1. サンプルYAML

- deployment.yml
- service.yml
- ingress.yml

deployment.ymlとserice.ymlについては、4_deployment-sericeで解説しています。  
ingress.ymlについてのみ説明します。
Ingressリソースのサンプルは以下の通りです。

```yaml
apiVersion: networking.k8s.io/v1
kind: Ingress
metadata:
  name: ingress
  namespace: default
spec:
  rules:
  - http:
      paths:
        - path: /
          pathType: Prefix
          backend:
            service:
              name: nginx-service
              port:
                number: 8088
```

Ingressリソースでは、SerivceのClusterIPアドレスと外部のネットワークを接続する設定を定義します。利用できるプロトコルはHTTPプロトコルでHTTPとHTTPS接続のロードバランサーを実現します。

![ingress](./images/ingress.png)

以下の図はK3SでデフォルトでインストールされるTraefikとKlipperによるIngressの構成図です。

![ingress traefik](./images/ingress-traefik.png)

Ingressにはハードウェアロードバランサーを利用する方法やCloudのManegedロードバランサーを使う方法があり、この場合利用したい構成に合わせたIngressコントローラーを導入する必要があります。K3SではこのIngressコントローラーとロードバランサーの両方をTraefikが担うのでIngressコントローラーを導入する必要はありません。ハードウェアロードバランサーやCloudのManegedロードバランサーの場合は、ロードバランサーの実体とIngressコントローラーが分かれる為、理解が難しいリソースの一つとなっています。

## 2. YAMLファイルヘッダー項目

```yaml
apiVersion: networking.k8s.io/v1
kind: Ingress
metadata:
  name: ingress
  namespace: default
```

kind項目が`Ingress`になっています。

## 3. リソース内容の記述

```yaml
spec:
  rules:
  - http:
      paths:
        - path: /
          pathType: Prefix
          backend:
            service:
              name: nginx-service
              port:
                number: 8088
```

rules項目でhttpリクエストでマッチするホスト名やパスを指定して、`Backend`でマッチしたリクエストを転送する先を指定する。

httpリクエストでマッチする部分
```yaml
  - host: hogegeho.example.jp
    http:
      paths:
        - path: /
          pathType: Prefix
```

`host:` でURLのホストでマッチするFQDN指定します。指定しない場合は、全てのFQDNにマッチします。

`paths:` でURLのパスにマッチするディレクトリパスを指定します。

`paths:`には`path:`と`pathType:`の2つを最低指定します。

`pathType:`には以下の3種類あります。


pathType | 詳細
---------|----------
ImplementationSpecific（実装に特有）| このパスタイプでは、パスとの一致はIngressClassに依存します。Ingressの実装はこれを独立したpathTypeと扱うことも、PrefixやExactと同一のパスタイプと扱うこともできます。
Exact | 大文字小文字を区別して完全に一致するURLパスと一致します。
Prefix | /で分割されたURLと前方一致で一致します。大文字小文字は区別され、パスの要素対要素で比較されます。パス要素は/で分割されたパスの中のラベルのリストを参照します。リクエストがパス p に一致するのは、Ingressのパス p がリクエストパス p と要素単位で前方一致する場合です。


### パスの指定例

| タイプ | パス                            | リクエストパス                | 一致するか                           |
|--------|---------------------------------|-------------------------------|--------------------------------------|
| Prefix | `/`                             | （全てのパス）                | はい                                 |
| Exact  | `/foo`                          | `/foo`                        | はい                                 |
| Exact  | `/foo`                          | `/bar`                        | いいえ                               |
| Exact  | `/foo`                          | `/foo/`                       | いいえ                               |
| Exact  | `/foo/`                         | `/foo`                        | いいえ                               |
| Prefix | `/foo`                          | `/foo`, `/foo/`               | はい                                 |
| Prefix | `/foo/`                         | `/foo`, `/foo/`               | はい                                 |
| Prefix | `/aaa/bb`                       | `/aaa/bbb`                    | いいえ                               |
| Prefix | `/aaa/bbb`                      | `/aaa/bbb`                    | はい                                 |
| Prefix | `/aaa/bbb/`                     | `/aaa/bbb`                    | はい、末尾のスラッシュは無視         |
| Prefix | `/aaa/bbb`                      | `/aaa/bbb/`                   | はい、末尾のスラッシュと一致         |
| Prefix | `/aaa/bbb`                      | `/aaa/bbb/ccc`                | はい、パスの一部と一致               |
| Prefix | `/aaa/bbb`                      | `/aaa/bbbxyz`                 | いいえ、接頭辞と一致しない           |
| Prefix | `/`, `/aaa`                     | `/aaa/ccc`                    | はい、接頭辞`/aaa`と一致             |
| Prefix | `/`, `/aaa`, `/aaa/bbb`         | `/aaa/bbb`                    | はい、接頭辞`/aaa/bbb`と一致         |
| Prefix | `/`, `/aaa`, `/aaa/bbb`         | `/ccc`                        | はい、接頭辞`/`と一致                |
| Prefix | `/aaa`                          | `/ccc`                        | いいえ、デフォルトバックエンドを使用 |
| Mixed  | `/foo` (Prefix), `/foo` (Exact) | `/foo`                        | はい、Exactが優先                    |

