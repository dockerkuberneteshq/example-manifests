# サービスサンプルYAMLについて

## 1. サンプルYAML

Serviceリソースのサンプルは以下の通りです。

```yaml
apiVersion: v1
kind: Service
metadata:
  name: sample-clusterip
spec:
  type: ClusterIP
  ports:
  - name: "http-port"
    protocol: "TCP"
    port: 8080
    targetPort: 80
  selector:
    app: sample-app
```

サービスリソースでは、DeploymentやReplicaset等のPodの接続ポイントを提供したり、ネットワークリクエストをコンテナへ転送する為のロードバランサー的な負荷分散機能を提供します。

## 2. YAMLファイルヘッダー項目

```yaml
apiVersion: v1
kind: Service
metadata:
  name: sample-clusterip
spec:
```

kind項目が`Service`になっています。

## 3. リソース内容の記述

```yaml
spec:
  type: ClusterIP
  ports:
  - name: "http-port"
    protocol: "TCP"
    port: 8080
    targetPort: 80
  selector:
    app: sample-app
```

spec直下の指定内容はまずtypeを指定してどの種類の接続を行うかを指定して、接続を受け付けるportとコンテナに転送するポートを指定します。最後にselectorでどのラベルが付いているコンテナに接続するかを指定します。

項目名 | 説明
----|------
 type | 接続種類を指定する
 port | 接続ポートを指定する
 selector | 接続先のコンテナのラベルを指定する

selectorで指定するのは、Deploymentリソースの名前ではなくて、podに付与されているラベルだという所が重要ポイントです。

4. 実行方法

kubectlで、上記のDeploymentとServiceをデプロイします。

```
kubectl apply -f ./sample-deployment.yaml
deployment.apps/sample-deployment created
service/sample-clusterip created
```

## 5. 実行確認

### 5-1. Service実行確認

serviceリソースが実行されているか確認する。
ClusterIPを指定しているのでKubernetes内のIPアドレスがServiceリソースに設定されているのが分かる。

```
$ kubectl get services
NAME               TYPE        CLUSTER-IP     EXTERNAL-IP   PORT(S)    AGE
kubernetes         ClusterIP   10.43.0.1      <none>        443/TCP    23h
sample-clusterip   ClusterIP   10.43.165.96   <none>        8080/TCP   75s
```

このIPは、他のKubernetesのPodから接続できる。

ポート情報などは以下のコマンドを実行する

```
$ kubectl describe service sample-clusterip
Name:              sample-clusterip
Namespace:         default
Labels:            <none>
Annotations:       <none>
Selector:          app=sample-app
Type:              ClusterIP
IP Family Policy:  SingleStack
IP Families:       IPv4
IP:                10.43.165.96
IPs:               10.43.165.96
Port:              http-port  8080/TCP
TargetPort:        80/TCP
Endpoints:         10.42.0.27:80,10.42.0.28:80,10.42.0.29:80
Session Affinity:  None
Events:            <none>
```

EndpointsのIPアドレスは、各PodのIPアドレスとなっています。

```
$ kubectl get pod -l app=sample-app -o wide
NAME                                 READY   STATUS    RESTARTS   AGE    IP           NODE        NOMINATED NODE   READINESS GATES
sample-deployment-77c7b569f6-l44mz   1/1     Running   0          8m6s   10.42.0.27   jetsonnx3   <none>           <none>
sample-deployment-77c7b569f6-gzdk5   1/1     Running   0          8m6s   10.42.0.29   jetsonnx3   <none>           <none>
sample-deployment-77c7b569f6-mnhbw   1/1     Running   0          8m6s   10.42.0.28   jetsonnx3   <none>           <none>
```

### 5-2. Service経由でのネットワーク接続確認



