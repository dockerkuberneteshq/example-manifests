# PodサンプルYAMLについて

## 1. サンプルYAML

Podリソースのサンプルは以下の通りです。

```yaml:sample-pod.yaml
apiVersion: v1
kind: Pod
metadata:
  name: sample-pod
spec:
  containers:
  - name: nginx-container
    image: nginx:1.16
```

## 2. YAMLファイルヘッダー項目

上記のYAMLで以下の5行は、ヘッダー項目でKubernetesのリソースを指定するYAMLには必ず入ります。  

```yaml
apiVersion: v1
kind: Pod
metadata:
  name: sample-pod
spec:
```

それぞれ以下の意味があります。

YAMLトップ項目| 詳細 
---------|----------
apiVersion: | Kubernetesで利用するAPIのバージョン
kind: | リソース種類(Pod,Deployment,ConfigMap等)
metadata: | このYAMLファイルの名前やラベル、Kubernetes側で自動的に設定される情報が書き込まれるフィールド
name: | このリソースの名前(任意名)[※利用可能な文字は英小文字、`-`、`,`]
spec: | このYAMLのkind: podの中身について、この配下に記述します

※**`注！`**:nameのPod名は、英小文字と数字と`'-'`、`','`のみ利用可能です。また、始まりと終わりは英小文字のみ利用可能です。


apiVersionは、複数あり同じリソース種別でもapiVersionが違うと別の書き方や項目が追加されることがあります。 
例として、ingressリソースはAPIのバージョンが違う2つの種類があります。

```
16:39 $ kubectl api-resources | sort | grep ingresses
ingresses                         ing          extensions/v1beta1                true         Ingress
ingresses                         ing          networking.k8s.io/v1              true         Ingress
``` 

最初は、`extensions/v1beta1`のAPIで実装がありましたが、正式なリソースとして`networking.k8s.io/v1`に移行中です。将来的に`extensions/v1beta1`のAPI側の`ingress`リソースは削除される予定です。

このように同じリソースでもv1とv2では書き方の指定が変わる可能性があります。

## 3. リソース内容の記述

以下の4行は、kind: podリソースの詳細内容について記述しています。

```yaml
spec:
  containers:
  - name: nginx-container
    image: nginx:1.16
```
  
それぞれ以下の意味があります
</br>

Pod項目|詳細
-------|--------
spec: | 項目識別子
containers: | kind: podで規定されているcontainerについての項目識別子
name: | このコンテナーの名前(任意名)
image: | DockerHub等のコンテナーレジストリーからダウンロードするDockerイメージ名を指定

specは項目の識別子なので、全てのリソースに必須項目です。\
このspec以下から詳細を記述するという目印になっています。

containersは、podリソースで規定されているコンテナーに関する情報を記載するという項目の識別子になっています。containers以下は、YAMLファイルの配列とハッシュで記載します。

<YAMLまめ知識>
* 配列とは？\
複数の値をまとめて記述する方法\
記載例:
  - test1
  - test2

上記はtest1,test2という複数の値を持つ配列

* ハッシュとは？\
キーとバリューのハッシュで記述する方法\
記載例:
- `name: test`
  
上記はkeyが`name`でvalueが`test`のハッシュ(連想配列)

<\/YAMLまめ知識>

## 4. 実行方法

`kubectl apply -f ./sample-pod.yaml`コマンドで実行する。

```
$ kubectl apply -f ./sample-pod.yaml 
pod/sample-pod created
```

createdとなっていれば、Kubernetesがyamlのファイルの構文に問題がなく実行を開始したことを示している。構文が問題なく、実行を開始されても記載に不備があるとコンテナーは起動しないのでご注意ください。

## 5. 実行確認

実行結果は以下のようになる\
`kubectl get pods`で取得できる

```bash
$ kubectl get pods sample-pod
NAME         READY   STATUS    RESTARTS   AGE
sample-pod   1/1     Running   0          41m
```

STATUSがRunningになっていればOKです。

## 6. 実行詳細表示

`kubectl describe pod sample-pod` コマンドで実行時の詳細を確認することができます。

```
17:10 $ kubectl describe pod sample-pod
Name:         sample-pod
Namespace:    default
Priority:     0
Node:         k3sup-test-agent/192.168.10.142
Start Time:   Tue, 27 Jul 2021 15:40:29 +0900
Labels:       <none>
Annotations:  <none>
Status:       Running
IP:           10.42.1.101
IPs:
  IP:  10.42.1.101
Containers:
  nginx-container:
    Container ID:   containerd://191fdcf81edd069ff0a961a3b8613f7931b7eb5af300c073a502e2d5dc88fcbc
    Image:          nginx:1.16
    Image ID:       docker.io/library/nginx@sha256:d20aa6d1cae56fd17cd458f4807e0de462caf2336f0b70b5eeb69fcaaf30dd9c
    Port:           <none>
    Host Port:      <none>
    State:          Running
      Started:      Tue, 27 Jul 2021 15:40:43 +0900
    Ready:          True
    Restart Count:  0
    Environment:    <none>
    Mounts:
      /var/run/secrets/kubernetes.io/serviceaccount from default-token-mhpjx (ro)
Conditions:
  Type              Status
  Initialized       True 
  Ready             True 
  ContainersReady   True 
  PodScheduled      True 
Volumes:
  default-token-mhpjx:
    Type:        Secret (a volume populated by a Secret)
    SecretName:  default-token-mhpjx
    Optional:    false
QoS Class:       BestEffort
Node-Selectors:  <none>
Tolerations:     node.kubernetes.io/not-ready:NoExecute op=Exists for 300s
                 node.kubernetes.io/unreachable:NoExecute op=Exists for 300s
Events:          <none>
```

## 7. 実行ログ確認

`kubectl log  sample-pod` コマンドで実行時の標準出力されたログを見ることができます。\
本アプリでは、ログが出力される状態にないので出力されません。

## 8. 削除方法

リソースは、`kubectl delete -f ./sample-pod.yaml`で削除できます。

```
$ kubectl delete -f ./sample-pod.yaml 
pod "sample-pod" deleted
```

yamlファイルがない場合は、`kubectl get pods`で取得したpod名を直接指定して以下のようにすれば削除できます。

```
$ kubectl delete pod sample-pod
pod "sample-pod" deleted
```

削除されたか確認

```
$ kubectl get pod sample-pod
Error from server (NotFound): pods "sample-pod" not found
``` 

注: Podの上位リソースであるReplicaSetやDeploymentによりPodが生成されている場合、Podを削除しても上位のリソースのReplicaSetやDeploymentによりPodが再生成されます。上位のリソースから削除してください。