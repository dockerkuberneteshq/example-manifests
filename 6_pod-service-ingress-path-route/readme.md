# サンプルYAMLについて

## 1. サンプルYAML

- ingress-pathRoute.yaml
- ingress-pathHost.yaml
- apple.yaml
- banana.yaml

apple.yamlとbanana.yamlについては、deploymentとserviceなので解説を省略します。ingress.ymlについてのみ説明します。
Ingressリソースのサンプルは以下の通りです。

1. URLのパスによるルーティング

```yaml
# ingress-pathRoute.yaml
apiVersion: networking.k8s.io/v1
kind: Ingress
metadata:
  name: example-ingress
spec:
  rules:
  - http:
      paths:
        - path: /apple
          pathType: Prefix
          backend:
            service:
              name: apple-service
              port:
                number: 8080
        - path: /banana
          pathType: Prefix
            service:
              name: banana-service
              port:
                number: 8080
```

2. ホストによるルーティング

```yaml
# ingress.yaml
apiVersion: networking.k8s.io/v1
kind: Ingress
metadata:
  name: example-ingress
  namespace: default
spec:
  rules:
  - host: echo-server  # host名はingressが実行されているところと合わせること
    http:
      paths:
        - host: apple.192.168.10.153.sslip.io
          path: /
          pathType: Prefix
          backend:
            service:
              name: apple-service
              port:
                number: 8080
        - host: apple.192.168.10.153.sslip.io
          path: /
          pathType: Prefix
            service:
              name: banana-service
              port:
                number: 8080
```

## 2. YAMLファイルヘッダー項目

```yaml
apiVersion: networking.k8s.io/v1
kind: Ingress
metadata:
  name: ingress
  namespace: default
```

kind項目が`Ingress`です。

## 3. リソース内容の記述

1. パスルーティング

```yaml
spec:
  rules:
  - host: test.example.jp  # host名はingressが実行されているところと合わせること
    http:
      paths:
        - path: /apple
          pathType: Prefix
          backend:
            service:
              name: apple-service
              port:
                number: 8080
        - path: /banana
          pathType: Prefix
            service:
              name: banana-service
              port:
                number: 8080
```

`spec.rules.http.paths.path`が`/apple`と`/banana`になっています。
これで、`https://test.example.jp/apple`と`https://test.example.jp/banana`にマッチします。

2. ホストルーティング

```yaml
spec:
  rules:
    http:
      paths:
        - host: apple.192.168.10.153.sslip.io
          path: /
          pathType: Prefix
          backend:
            service:
              name: apple-service
              port:
                number: 8080
        - host: banana.192.168.10.153.sslip.io
          path: /
          pathType: Prefix
            service:
              name: banana-service
              port:
                number: 8080
```

`spec.rules.http.paths.host`が`apple.192.168.10.153.sslip.io`と`banana.192.168.10.153.sslip.io`になっています。
これで、`https://apple.192.168.10.153.sslip.io`と`https://banana.192.168.10.153.sslip.io`にマッチします。

