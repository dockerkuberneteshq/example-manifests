# ReplicaSetサンプルYAMLについて
## 1. サンプルYAML

ReplicaSetリソースのサンプルは以下の通りです。

```yaml:sample-rs.yaml
apiVersion: apps/v1
kind: ReplicaSet
metadata:
  name: sample-rs
spec:
  replicas: 3
  selector:
    matchLabels:
      app: sample-app
  template:
    metadata:
      labels:
        app: sample-app
    spec:
      containers:
      - name: nginx-container
        image: nginx:1.16
```

## 2. YAMLファイルヘッダー項目

PodのYAMLファイルと同じで上記のYAMLで以下の5行は、ヘッダー項目でKubernetesのリソースを指定するYAMLには必ず入ります。

```yaml:sample-rs.yaml
apiVersion: apps/v1
kind: ReplicaSet
metadata:
  name: sample-rs
spec:
```

それぞれ以下の意味があります。

YAMLトップ項目| 詳細 
---------|----------
apiVersion: | Kubernetesで利用するAPIのバージョン
kind: | リソース種類(Pod,Deployment,ConfigMap等)
metadata: | このYAMLファイルの名前やラベル、Kubernetes側で自動的に設定される情報が書き込まれるフィールド
name: | このリソースの名前(任意名)[※利用可能な文字は英小文字、`-`、`,`]
spec: | このYAMLのkind: `ReplicaSet`の中身について、この配下に記述します

※**`注！`**:nameのReplicaSet名は、英小文字と数字と`'-'`、`','`のみ利用可能です。また、始まりと終わりは英小文字のみ利用可能です。


## 3. リソース内容の記述

```yaml
spec:
  replicas: 3
  selector:
    matchLabels:
      app: sample-app
  template:
    metadata:
      labels:
        app: sample-app
    spec:
      containers:
      - name: nginx-container
        image: nginx:1.16
```

それぞれ以下の意味があります
</br>

ReplicaSet項目|詳細
-------|--------
spec: | 項目識別子
replicas: | <数値>,templateのコンテナーをいくつ生成するか指定
selector: | Podのlabel(metadata.labels)がselectorで指定したlabelと同じものが付いているものをこのReplicaSetのグループとする指定
template: | Podを実行するマニフェストを記載<br>具体的にはPodリソースのapiVersion,kindより下の行を記載する

* 注:template:より下は、Podリソースの記述方法と同じになります。


## 4. 実行方法

`kubectl apply -f ./sample-rs.yaml` コマンドで実行します。

```
$ kubectl apply -f ./sample-rs.yaml 
replicaset.apps/sample-rs created
```

## 5. 実行確認
### 5-1. Pod実行確認

ReplicaSet内のPodが実行されているかどうか確認する

`kubectl get pods -L app -l app=sample-app` コマンドでReplicaSetによるPodの実行を確認する。

```
$ kubectl get pods -L app -l app=sample-app
NAME              READY   STATUS    RESTARTS   AGE   APP
sample-rs-cbrds   1/1     Running   0          33m   sample-app
sample-rs-ksfmf   1/1     Running   0          33m   sample-app
sample-rs-zqtvw   1/1     Running   0          33m   sample-app
```

`-L` は付いているラベルを表示するオプション
`-l app=sample-app` は指定のラベルのみ表示するオプション

### 5-2. ReplicaSet実行確認

`kubectl get rs sample-rs` コマンドでReplicaSetの実行を確認できる。

```
$ kubectl get rs sample-rs
NAME        DESIRED   CURRENT   READY   AGE
sample-rs   3         3         3       40m
```

項目名|詳細
----|-----
DESIRED|Podが起動される予定数を示す
CURRENT|Pod実行数を示す
READY|利用可能数を示す

</br>


### 5-3. ReplicaSet実行詳細

`kubectl describe rs sample-rs` コマンドでReplicaSetの詳細を確認できる。

```
$ kubectl describe rs sample-rs
Name:         sample-rs
Namespace:    default
Selector:     app=sample-app
Labels:       <none>
Annotations:  <none>
Replicas:     3 current / 3 desired
Pods Status:  3 Running / 0 Waiting / 0 Succeeded / 0 Failed
Pod Template:
  Labels:  app=sample-app
  Containers:
   nginx-container:
    Image:        nginx:1.16
    Port:         <none>
    Host Port:    <none>
    Environment:  <none>
    Mounts:       <none>
  Volumes:        <none>
Events:
  Type    Reason            Age   From                   Message
  ----    ------            ----  ----                   -------
  Normal  SuccessfulCreate  42m   replicaset-controller  Created pod: sample-rs-ksfmf
  Normal  SuccessfulCreate  42m   replicaset-controller  Created pod: sample-rs-cbrds
  Normal  SuccessfulCreate  42m   replicaset-controller  Created pod: sample-rs-zqtvw
```

## 6. 削除方法

`kubectl delete -f ./sample-rs.yaml`コマンドで削除できます。

```
$ kubectl delete -f ./sample-rs.yaml 
replicaset.apps "sample-rs" deleted
```

yamlファイルがない場合は、以下の方法で削除できます。

```
$ kubectl delete  rs sample-rs
```

削除確認

```
$ kubectl get rs sample-rs
Error from server (NotFound): replicasets.apps "sample-rs" not found
```

