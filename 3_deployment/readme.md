# DeploymentサンプルYAMLについて
## 1. サンプルYAML

Deploymentリソースのサンプルは以下の通りです

```yaml:sample-deployment.yaml
apiVersion: apps/v1
kind: Deployment
metadata:
  name: sample-deployment
spec:
  replicas: 3
  selector:
    matchLabels:
      app: sample-app
  template:
    metadata:
      labels:
        app: sample-app
    spec:
      containers:
      - name: nginx-container
        image: nginx:1.16
```

## 2. YAMLヘッダー項目

ReplicasetのYAMLファイルと同じで以下の5行は、ヘッダー項目でKubernetesのリソースを指定するYAMLには必ず入ります。

```yaml:sample-deployment.yaml
apiVersion: apps/v1
kind: Deployment
metadata:
  name: sample-deployment
spec:
```

replicasetと違うのはKindが`Deployment`になっていることです。

## 3. リソース内容の記述

```yaml
spec:
  replicas: 3
  selector:
    matchLabels:
      app: sample-app
  template:
    metadata:
      labels:
        app: sample-app
    spec:
      containers:
      - name: nginx-container
        image: nginx:1.16
```

上記のリソースファイルを見たところ、replicasetと全く同じです。しかし、Deploymentリソースで説明した通りDeploymentはReplicasetをバージョンアップする機能があります。

![Deployment](./images/deployment-slide.png)


## 4. 実行方法

kubectlで、上記のDeploymentをデプロイします
この時に`--record`というアップデートを付けるとロールバックが可能になります。ロールバックする必要がないのであれば付ける必要はありません。今回はロールバックを試してみたいのでこのオプションを付けておきます。

```
kubectl apply -f sample-deployment.yaml --record
```

## 5. 実行確認
### 5-1. Pod実行確認

Deployment内のPodが実行されているかどうか確認する
`kubectl get pods -L app -l app=sample-app`コマンドでDeploymentによるPodの実行を確認する。

```
NAME                                 READY   STATUS    RESTARTS   AGE   APP
sample-deployment-77c7b569f6-94s4g   1/1     Running   0          40s   sample-app
sample-deployment-77c7b569f6-f8rsk   1/1     Running   0          40s   sample-app
sample-deployment-77c7b569f6-c4xvp   1/1     Running   0          40s   sample-app
```

`-L` は付いているラベルを表示するオプション
`-l app=sample-app` は指定のラベルのみ表示するオプション

### 5-2. Deploymentのコンテナイメージを更新する

sample-deployment.yamlのコンテナイメージは`nginx:1.16`でnginxのバージョン 1.16でした。これをバージョン 1.17に変更してみます。

```
kubectl set image deployment sample-deployment nginx-container=nginx:1.17 --record
```

アップデート前のdescribe

```
$ kubectl describe deployment sample-deployment
Name:                   sample-deployment
Namespace:              default
CreationTimestamp:      Wed, 27 Apr 2022 13:36:31 +0900
Labels:                 <none>
Annotations:            deployment.kubernetes.io/revision: 1
                        kubernetes.io/change-cause: kubectl apply --filename=sample-deployment.yaml --record=true
Selector:               app=sample-app
Replicas:               3 desired | 3 updated | 3 total | 3 available | 0 unavailable
StrategyType:           RollingUpdate
MinReadySeconds:        0
RollingUpdateStrategy:  25% max unavailable, 25% max surge
Pod Template:
  Labels:  app=sample-app
  Containers:
   nginx-container:
    Image:        nginx:1.16
    Port:         <none>
    Host Port:    <none>
    Environment:  <none>
    Mounts:       <none>
  Volumes:        <none>
Conditions:
  Type           Status  Reason
  ----           ------  ------
  Available      True    MinimumReplicasAvailable
  Progressing    True    NewReplicaSetAvailable
OldReplicaSets:  <none>
NewReplicaSet:   sample-deployment-77c7b569f6 (3/3 replicas created)
Events:
  Type    Reason             Age   From                   Message
  ----    ------             ----  ----                   -------
  Normal  ScalingReplicaSet  10m   deployment-controller  Scaled up replica set sample-deployment-77c7b569f6 to 3
```

デプロイ中

```
$ kubectl describe deployment sample-deployment
Name:                   sample-deployment
Namespace:              default
CreationTimestamp:      Wed, 27 Apr 2022 13:36:31 +0900
Labels:                 <none>
Annotations:            deployment.kubernetes.io/revision: 2
                        kubernetes.io/change-cause: kubectl set image deployment sample-deployment nginx-container=nginx:1.17 --record=true
Selector:               app=sample-app
Replicas:               3 desired | 1 updated | 4 total | 3 available | 1 unavailable
StrategyType:           RollingUpdate
MinReadySeconds:        0
RollingUpdateStrategy:  25% max unavailable, 25% max surge
Pod Template:
  Labels:  app=sample-app
  Containers:
   nginx-container:
    Image:        nginx:1.17
    Port:         <none>
    Host Port:    <none>
    Environment:  <none>
    Mounts:       <none>
  Volumes:        <none>
Conditions:
  Type           Status  Reason
  ----           ------  ------
  Available      True    MinimumReplicasAvailable
  Progressing    True    ReplicaSetUpdated
OldReplicaSets:  sample-deployment-77c7b569f6 (3/3 replicas created)
NewReplicaSet:   sample-deployment-7d47f997b7 (1/1 replicas created)
Events:
  Type    Reason             Age   From                   Message
  ----    ------             ----  ----                   -------
  Normal  ScalingReplicaSet  11m   deployment-controller  Scaled up replica set sample-deployment-77c7b569f6 to 3
  Normal  ScalingReplicaSet  6s    deployment-controller  Scaled up replica set sample-deployment-7d47f997b7 to 1
```

デプロイ後のdescribe

```
$ kubectl describe deployment sample-deployment
Name:                   sample-deployment
Namespace:              default
CreationTimestamp:      Wed, 27 Apr 2022 13:36:31 +0900
Labels:                 <none>
Annotations:            deployment.kubernetes.io/revision: 2
                        kubernetes.io/change-cause: kubectl set image deployment sample-deployment nginx-container=nginx:1.17 --record=true
Selector:               app=sample-app
Replicas:               3 desired | 3 updated | 3 total | 3 available | 0 unavailable
StrategyType:           RollingUpdate
MinReadySeconds:        0
RollingUpdateStrategy:  25% max unavailable, 25% max surge
Pod Template:
  Labels:  app=sample-app
  Containers:
   nginx-container:
    Image:        nginx:1.17
    Port:         <none>
    Host Port:    <none>
    Environment:  <none>
    Mounts:       <none>
  Volumes:        <none>
Conditions:
  Type           Status  Reason
  ----           ------  ------
  Available      True    MinimumReplicasAvailable
  Progressing    True    NewReplicaSetAvailable
OldReplicaSets:  <none>
NewReplicaSet:   sample-deployment-7d47f997b7 (3/3 replicas created)
Events:
  Type    Reason             Age   From                   Message
  ----    ------             ----  ----                   -------
  Normal  ScalingReplicaSet  13m   deployment-controller  Scaled up replica set sample-deployment-77c7b569f6 to 3
  Normal  ScalingReplicaSet  2m7s  deployment-controller  Scaled up replica set sample-deployment-7d47f997b7 to 1
  Normal  ScalingReplicaSet  111s  deployment-controller  Scaled down replica set sample-deployment-77c7b569f6 to 2
  Normal  ScalingReplicaSet  111s  deployment-controller  Scaled up replica set sample-deployment-7d47f997b7 to 2
  Normal  ScalingReplicaSet  109s  deployment-controller  Scaled down replica set sample-deployment-77c7b569f6 to 1
  Normal  ScalingReplicaSet  109s  deployment-controller  Scaled up replica set sample-deployment-7d47f997b7 to 3
  Normal  ScalingReplicaSet  107s  deployment-controller  Scaled down replica set sample-deployment-77c7b569f6 to 0
```

Imageがnginx:1.17になっているのが分かると思います。

### 5-3. ロールバックする

更新したバージョンにバグがあり元に戻す場合には`rollback`を利用します。

変更履歴を確認します

```
$ kubectl rollout history deployment sample-deployment
deployment.apps/sample-deployment
REVISION  CHANGE-CAUSE
1         kubectl apply --filename=sample-deployment.yaml --record=true
2         kubectl set image deployment sample-deployment nginx-container=nginx:1.17 --record=true
```

リビジョンの1と2があります。リビジョンを指定する事も可能ですが、何も指定しない場合は一つ前に戻ります。
以下のコマンドで1つリビジョンをロールバックします。

```
kubectl rollout undo deployment sample-deployment
deployment.apps/sample-deployment rolled back
```

ロールバック中のPodの数の推移です。

ロールバック前
```
$ kubectl get replicasets
NAME                           DESIRED   CURRENT   READY   AGE
sample-deployment-7d47f997b7   3         3         3       9m12s
sample-deployment-77c7b569f6   0         0         0       20m
```

ロールバックコマンド実行
```
$ kubectl rollout undo deployment sample-deployment
deployment.apps/sample-deployment rolled back
```

ロールバック中

```
$ kubectl get replicasets
NAME                           DESIRED   CURRENT   READY   AGE
sample-deployment-7d47f997b7   2         2         2       9m42s
sample-deployment-77c7b569f6   2         2         1       21m
$ kubectl get replicasets
NAME                           DESIRED   CURRENT   READY   AGE
sample-deployment-7d47f997b7   1         1         1       9m44s
sample-deployment-77c7b569f6   3         3         2       21m
$ kubectl get replicasets
NAME                           DESIRED   CURRENT   READY   AGE
sample-deployment-7d47f997b7   1         1         1       9m45s
sample-deployment-77c7b569f6   3         3         2       21m
```

ロールバック終了

```
$ kubectl get replicasets
NAME                           DESIRED   CURRENT   READY   AGE
sample-deployment-77c7b569f6   3         3         3       21m
sample-deployment-7d47f997b7   0         0         0       9m47s
```

見て分かるとおり、片方(nginx:1.16)のコンテナが増えていく途中で、もう片方(nginx:1.17)のコンテナが減っていき、最終的にnginx:1.16のコンテナが3つになってロールバックが終了しました。

# 6. 削除方法


`kubectl delete -f ./sample-deployment.yaml` コマンドで削除できます。

```bash
$ kubectl delete -f ./sample-deployment.yaml
deployment.apps "sample-deployment" deleted
```

yamlファイルがない場合は、以下の方法で削除できます。
```
kubectl delete deploy sample-deployment
```

削除確認

```
$ kubectl get deploy sample-deployment
Error from server (NotFound): deployments.apps "sample-deployment" not found
```
